#pragma once

template<typename T> class Stack
{
private:
	struct Node {
		T* value;
		Node* prev;

		Node(T* value, Node* prev);
	};

	Node* last;
	int size;

public:
	Stack();
	Stack(T* item);
	~Stack();

	void empty();
	void push(T* item);
	T* pop();
	
	int getSize() { return this->size; }
	bool isEmpty() { return this->last == nullptr; }
};

template<typename T>
inline Stack<T>::Stack()
{
	this->last = nullptr;
	size = 0;
}

template<typename T>
inline Stack<T>::Stack(T* item)
{
	this->last = new Node(item, nullptr);
	this->size = 0;
}

template<typename T>
inline Stack<T>::~Stack()
{
	this->empty();
}

template<typename T>
inline void Stack<T>::empty()
{
	while (!this->isEmpty()) {
		Node* elem = this->last;
		this->last = this->last->prev;
		delete elem;
	}
}

template<typename T>
inline void Stack<T>::push(T* item)
{
	Node* elem = new Node(item, this->last);
	this->last = elem;
	this->size++;
}

template<typename T>
inline T* Stack<T>::pop()
{
	if (this->isEmpty()) {
		throw std::string("Stack is empty");
	}

	Node* elem = this->last;
	T* value = this->last->value;

	this->last = this->last->prev;
	delete elem;

	this->size--;

	return value;
}

template<typename T>
inline Stack<T>::Node::Node(T* value, Node* prev)
{
	this->value = value;
	this->prev = prev;
}