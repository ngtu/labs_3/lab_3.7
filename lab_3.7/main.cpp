﻿#include <iostream>
#include <fstream>
#include "DateTime.h"
#include "Stack.h"
#include "Error.h"

void runApp();

int main()
{
    try {
        runApp();
    }
    catch (Error e) {
        std::cout << "[ERROR]: " << e.getMessage() << '\n';
        return e.getCode();
    }

    return 0;
}

void runApp() {
    Stack<int> st1 = Stack<int>();
    st1.push(new int(1));
    st1.push(new int(2));
    st1.push(new int(3));

    std::cout << "Stack<int>: ";
    while (!st1.isEmpty()) {
        std::cout << *(st1.pop()) << ' ';
    }
    std::cout << "\n------------------------------\n";

    Stack<double> st2 = Stack<double>();
    st2.push(new double(1.1));
    st2.push(new double(2.2));
    st2.push(new double(3.3));

    std::cout << "Stack<double>: ";
    while (!st2.isEmpty()) {
        std::cout << *(st2.pop()) << ' ';
    }
    std::cout << "\n------------------------------\n";

    Stack<DateTime> st3 = Stack<DateTime>();
    st3.push(new DateTime(2023, 1, 1, 12, 15));
    st3.push(new DateTime(2023, 1, 2, 11, 55));
    st3.push(new DateTime(2023, 1, 1, 12, 15));

    std::cout << "Stack<DateTime>:\n";
    while (!st3.isEmpty()) {
        std::cout << st3.pop()->toStr() << '\n';
    }
}